#include <assert.h>
#include <iostream>
#include <mpi.h>
#include <vector>

void fill_buffer(std::vector<int> &buf) {
  for (auto &v : buf) {
    v = 0;
  }
}

int main() {
  int rank, size;
  std::vector<int> buf(100);

  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  assert(size == 2 && "Works only with 2 procs");

  if (rank == 0) {
    fill_buffer(buf);
    MPI_Ssend(buf.data(), buf.size(), MPI_INT, 1, 0, MPI_COMM_WORLD);
  } else if (rank == 1) {
    MPI_Recv(buf.data(), buf.size(), MPI_INT, 0, 0, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
  }

  MPI_Finalize();

  return 0;
}
