#include <array>
#include <cmath>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <string>
#include <tuple>

#ifndef PRINT_SIZE_H_
#define PRINT_SIZE_H_

template <class T>
inline std::string printHuman(T size, std::string unit, char base = 10) {
  int mult = 0;
  int fact = base == 10 ? 3 : 10;
  if (size != 0) {
    mult = std::floor((std::log(double(size)) / std::log(double(base))) / fact);
  }

  auto real_size = double(size) / double(std::pow(base, fact * mult));
  std::array<std::string, 12> ratio = {"n", u8"μ", "m", "",  "K", "M",
                                       "G", "T", "P", "E", "Z", "Y"};

  std::string base_2_correction;
  if (base == 2 and ratio[mult + 3] != "") {
    base_2_correction = "i";
  }

  std::stringstream sstr;
  sstr << std::fixed << std::setprecision(1) << real_size << ratio[mult + 3]
       << base_2_correction << unit;
  return sstr.str();
}

#endif // PRINT_SIZE_H_
