#include <iostream>
#include <mpi.h>
#include <vector>

int main() {
  MPI_Init(NULL, NULL);

  int prank;
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);

  MPI_Count lb, extent, size;

  struct Test_t {
    double d[2];
    int i;
  };

  std::vector<Test_t> foo(100);

  std::array<int, 2> block_lengths = {2, 1};
  std::array<MPI_Aint, 2> displacements;
  std::array<MPI_Datatype, 2> old_types = {MPI_DOUBLE, MPI_INT};

  MPI_Aint addr0, addr1;
  MPI_Get_address(&foo[0], &addr0);
  MPI_Get_address(&foo[0].d, &displacements[0]);
  MPI_Get_address(&foo[0].i, &displacements[1]);

  displacements[0] = MPI_Aint_diff(displacements[0], addr0);
  displacements[1] = MPI_Aint_diff(displacements[1], addr0);

  MPI_Datatype mpi_test_t, mpi_test_vector_t;

  MPI_Type_create_struct(2, block_lengths.data(), displacements.data(),
                         old_types.data(), &mpi_test_t);

  MPI_Get_address(&foo[1], &addr1);
  addr1 = MPI_Aint_diff(addr1, addr0);

  MPI_Type_create_resized(mpi_test_t, 0, addr1, &mpi_test_vector_t);
  MPI_Type_commit(&mpi_test_vector_t);

  MPI_Type_get_extent_x(mpi_test_t, &lb, &extent);
  MPI_Type_size_x(mpi_test_t, &size);
  std::cout << "MPI Datatype: mpi_test_t, size: " << size
            << " extent: " << extent << std::endl;
  std::cout << "C++ Datatype: Test_t, size: " << sizeof(Test_t) << std::endl;

  MPI_Type_free(&mpi_test_vector_t);
  MPI_Finalize();

  return 0;
}
