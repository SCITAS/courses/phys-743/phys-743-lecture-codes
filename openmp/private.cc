#include <iostream>
#include <omp.h>

int main() {
  int a = 1, b = 2;
  double c = 3.;

  std::printf("Thread %i sees, a, b, c: %i, %i, %g (before)\n",
              omp_get_thread_num(), a, b, c);

#pragma omp parallel num_threads(3), private(a), firstprivate(b)
  {
    std::printf("Thread %i sees, a, b, c: %i, %i, %g (inside)\n",
                omp_get_thread_num(), a, b, c);
    c = -1e-3;
  }

  std::printf("Thread %i sees, a, b, c: %i, %i, %g (after)\n",
              omp_get_thread_num(), a, b, c);

  return 0;
}
