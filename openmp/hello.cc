#include <iostream>
#include <omp.h>

int main() {

#pragma omp parallel
  {
    auto mysize = omp_get_num_threads();
    auto myrank = omp_get_thread_num();
    std::printf("Hello from thread %i out of %i\n", myrank, mysize);
  }

  return 0;
}
