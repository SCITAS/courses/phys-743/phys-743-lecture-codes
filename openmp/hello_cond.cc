#include <iostream>
#if defined(_OPENMP)
#  include <omp.h>
#endif

int main() {
  int mysize = 1;
  int myrank = 0;

#if defined(_OPENMP)
#pragma omp parallel
  {
    mysize = omp_get_num_threads();
    myrank = omp_get_thread_num();
#endif
    std::printf("Hello from thread %i out of %i\n", myrank, mysize);
#if defined(_OPENMP)
  }
#endif
  return 0;
}
