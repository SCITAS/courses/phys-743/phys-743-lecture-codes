#include <iostream>
#include <omp.h>

int main() {

#pragma omp parallel sections num_threads(4)
  {
#pragma omp section
    std::printf("Thread %i handling section 1\n", omp_get_thread_num());
#pragma omp section
    std::printf("Thread %i handling section 2\n", omp_get_thread_num());
#pragma omp section
    std::printf("Thread %i handling section 3\n", omp_get_thread_num());
  }

  return 0;
}
