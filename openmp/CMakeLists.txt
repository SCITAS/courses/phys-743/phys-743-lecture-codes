find_package(OpenMP REQUIRED COMPONENTS CXX)

foreach(tgt hello hello_cond sections private for dgemm)
  add_executable(${tgt} ${tgt}.cc)
  target_link_libraries(${tgt} OpenMP::OpenMP_CXX)
endforeach()

add_executable(hello_cond_wo_openmp hello_cond.cc)
